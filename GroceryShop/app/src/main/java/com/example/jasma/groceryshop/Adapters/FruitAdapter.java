package com.example.jasma.groceryshop.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jasma.groceryshop.Models.GroceryModel;
import com.example.jasma.groceryshop.Views.AnimationActivity;
import com.example.jasma.groceryshop.Views.ItemDetails;
import com.example.jasma.groceryshop.R;
import com.example.jasma.groceryshop.Views.ShoppingCart;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FruitAdapter extends
        RecyclerView.Adapter<FruitAdapter.ViewHolder> {
    int prevPosition = 0;
    private boolean quantityOrdered;
    List<GroceryModel> itemInfoArrayList;
    Context context;

    /* constructor of FruitAdapter class  */
    public FruitAdapter(List<GroceryModel> itemInfoArrayList, boolean quantityOrdered) {
        this.itemInfoArrayList = itemInfoArrayList;
        this.quantityOrdered = quantityOrdered;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_item_layout, parent, false);
        context = parent.getContext();
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }
/*populating data through holder*/
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GroceryModel groceryModel = itemInfoArrayList.get(position);
        holder.tvName.setText(groceryModel.getName());
        holder.tvPrice.setText(String.valueOf(groceryModel.price));

        Picasso.with(context).load(groceryModel.image).resize(180, 180).into(holder.imageView);
        if (position < prevPosition) {
            AnimationActivity.animateItems(holder, false);
        } else {
            AnimationActivity.animateItems(holder, true);
        }
        prevPosition = position;
          /* creating on click event for the  recycler view using using layout id
        and  using intent to send data to next activity*/
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newIntent = new Intent(context, ItemDetails.class);
                newIntent.putExtra("items", position);
                context.startActivity(newIntent);

            }
        });
    }
/* return size of list*/

    @Override
    public int getItemCount() {

        return itemInfoArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPrice, tvName, tvCurrency, quantity;
        ImageView imageView;
        LinearLayout mainLayout;
/*getting views by their ID's */
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.list_image1);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            tvPrice = itemView.findViewById(R.id.price);
            tvName = itemView.findViewById(R.id.viewName);

            tvCurrency = itemView.findViewById(R.id.dollar);
        }

    }
}
