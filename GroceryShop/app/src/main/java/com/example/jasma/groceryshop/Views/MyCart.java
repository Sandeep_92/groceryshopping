package com.example.jasma.groceryshop.Views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jasma.groceryshop.Adapters.CartAdapter;
import com.example.jasma.groceryshop.Models.CartModel;
import com.example.jasma.groceryshop.Models.GroceryModel;
import com.example.jasma.groceryshop.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MyCart extends AppCompatActivity {
    private List<GroceryModel> groceryModelList;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    TextView shoppingBill;
    TextView price;
    double totalBill = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        mRecyclerView=findViewById(R.id.cartList);
        groceryModelList = ShoppingCart.getCartItemList();
        mAdapter = new CartAdapter(groceryModelList,true);
        LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(getApplicationContext());
        verticalLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(verticalLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        price=findViewById(R.id.totalItemPrice);

    }
    @Override
    protected void onResume(){
        super.onResume();

        for(GroceryModel item : groceryModelList) {
            int  quantity = ShoppingCart.getItemQuantity(item);
            totalBill += item.price * quantity;
        }
        shoppingBill= findViewById(R.id.totalItemPrice);
        shoppingBill.setText(getString(R.string.totalBill) + totalBill);
    }
    /* on click method for more chopping*/
    public void continueShop(View view) {
        Intent intent=new Intent(MyCart.this,MainActivity.class);
        startActivity(intent);
    }

}


