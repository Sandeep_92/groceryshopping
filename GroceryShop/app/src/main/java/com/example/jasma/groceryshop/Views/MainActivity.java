package com.example.jasma.groceryshop.Views;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.jasma.groceryshop.Models.UserModel;
import com.example.jasma.groceryshop.R;
import com.example.jasma.groceryshop.Views.CallActivity;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String PREFS_NAME = "preference";
    UserModel userModel = new UserModel();
    String userIdentity;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    ArrayList<UserModel> userModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Log.i("testedit", editor.toString());

        userIdentity = getIntent().getStringExtra("userId");

        try {
            userModel = (UserModel) getIntent().getSerializableExtra("userModel");
            Log.i("testUser: ", userModel.toString());
            userIdentity = userModel.getUserId();
        } catch (Exception e) {

        }

        getUser(userIdentity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       /* on click for cart Activity*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent shoppingCartIntent = new Intent(getBaseContext(), MyCart.class);
               startActivity(shoppingCartIntent);

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    void getUser(String uid) {
        FirebaseFirestore.getInstance().collection("Users")
                .whereEqualTo("uid", uid)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots != null) {
                            userModelArrayList.clear();
                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                userModelArrayList.add(documentSnapshot.toObject(UserModel.class));
                            }
                            if (userModelArrayList.size() == 0) {
                                return;
                            } else {
                                userModel = userModelArrayList.get(0);
                            }
                        } else {
                            return;
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_profile) {

            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            intent.putExtra("userModel", userModel);
            startActivity(intent);


        }else if (id == R.id.nav_call) {
            Intent intent = new Intent(getApplicationContext(), CallActivity.class);
            startActivity(intent);

        }
        else if (id == R.id.nav_cart) {

           Intent intent = new Intent(getApplicationContext(), MyCart.class);
           startActivity(intent);


        }
        else if (id == R.id.nav_logout) {
            sharedPreferences = getSharedPreferences("MyPreference", 0);
            SharedPreferences.Editor e = sharedPreferences.edit();
            e.clear();
            e.apply();
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            startActivity(intent);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
