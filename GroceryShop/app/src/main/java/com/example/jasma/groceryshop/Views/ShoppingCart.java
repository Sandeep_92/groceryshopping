package com.example.jasma.groceryshop.Views;

import com.example.jasma.groceryshop.Models.CartModel;
import com.example.jasma.groceryshop.Models.GroceryModel;
import com.example.jasma.groceryshop.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart {
    private static List<GroceryModel> itemInfoList;
    private static Map<GroceryModel, CartModel> mapCartItems = new HashMap<GroceryModel, CartModel>();
/*method to get the list of all items*/
    public static List<GroceryModel> getItemList() {
        if (itemInfoList == null) {
            itemInfoList = new ArrayList<>();
            itemsData();
        }
        return itemInfoList;
    }
    /* method to set the item quantity for specific item */
    public static void setItemQuantity(GroceryModel item, int quantity) {
        CartModel currentItem = mapCartItems.get(item);
        if (currentItem == null) {
            currentItem = new CartModel(item, quantity);
            mapCartItems.put(item, currentItem);
            return;
        }
        currentItem.setQuantity(quantity);
    }
    /* method to get the item quantity for specific item */
    public static int getItemQuantity(GroceryModel product) {
        CartModel curEntry = mapCartItems.get(product);
        if (curEntry != null)
            return curEntry.getQuantity();
        return 0;
    }
/* method to add items in in list*/
    public static void itemsData() {
        itemInfoList.add(new GroceryModel("Apple", R.drawable.apple, 3.4));
        itemInfoList.add(new GroceryModel("Banana", R.drawable.banana, 3));
        itemInfoList.add(new GroceryModel("Grapes", R.drawable.grapes, 3.2));
        itemInfoList.add(new GroceryModel("Mango", R.drawable.mango, 3));
        itemInfoList.add(new GroceryModel("Orange", R.drawable.orange, 4));
        itemInfoList.add(new GroceryModel("Strawberry", R.drawable.strawberry, 3));
        itemInfoList.add(new GroceryModel("Kiwi", R.drawable.kivi, 2));
        itemInfoList.add(new GroceryModel("Yogurt", R.drawable.yogurt, 3));
        itemInfoList.add(new GroceryModel("Butter", R.drawable.butter, 2.3));
        itemInfoList.add(new GroceryModel("Cheese", R.drawable.cheese, 3));
        itemInfoList.add(new GroceryModel("Milk", R.drawable.milk, 2.3));
        itemInfoList.add(new GroceryModel("Ice Cream", R.drawable.ice_cream, 6));
        itemInfoList.add(new GroceryModel("Broccoli", R.drawable.broccoli, 3));
        itemInfoList.add(new GroceryModel("Capsicum", R.drawable.capsicum, 4));
        itemInfoList.add(new GroceryModel("Carrot", R.drawable.carrot, 2));
        itemInfoList.add(new GroceryModel("Cauliflower", R.drawable.cauliflower, 2));
        itemInfoList.add(new GroceryModel("GreenPeas", R.drawable.greenpees, 7));
        itemInfoList.add(new GroceryModel("Okra", R.drawable.okra, 2));

    }

/* method to get the items in cart*/
    public static List<GroceryModel> getCartItemList() {
        int listSize = mapCartItems.keySet().size();
        List<GroceryModel> cartList = new ArrayList<>(listSize);
        for (GroceryModel item : mapCartItems.keySet()) {
            cartList.add(item);
        }
        return cartList;
    }

}
