package com.example.jasma.groceryshop.Views;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jasma.groceryshop.Models.GroceryModel;
import com.example.jasma.groceryshop.R;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ItemDetails extends AppCompatActivity {
    TextView itemName, itemPrice, total;
    ImageView itemImage;
    EditText totalQuantity;
    GroceryModel groceryModel;
    int itemQuantity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        Views();
        List<GroceryModel> groceryModelList= ShoppingCart.getItemList();
        Intent detailsIntent=getIntent();
        int itemPosition=detailsIntent.getExtras().getInt("items");
        groceryModel = groceryModelList.get(itemPosition);
        itemImage.setImageResource(groceryModel.image);
        itemPrice.setText(String.valueOf(groceryModel.price));
        itemName.setText(groceryModel.name);

    }

    /* method to find app the views by Id's*/
    public void Views() {
        itemName = findViewById(R.id.itemName);
        itemPrice = findViewById(R.id.itemPrice);
        itemImage = findViewById(R.id.itemImage);
        total = findViewById(R.id.price);
        totalQuantity = findViewById(R.id.quantity);
    }

    /* method  for getting and setting quantity value in cart */

    public void addToCart(View view) {
        try {
            itemQuantity = Integer.parseInt(totalQuantity.getText().toString());
            if (itemQuantity <= 0) {
                Toast.makeText(getApplicationContext(), "Enter valid quantity", Toast.LENGTH_LONG).show();
                return;
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Please enter valid quantity",Toast.LENGTH_LONG).show();
            return;
        }
        ShoppingCart.setItemQuantity(groceryModel, itemQuantity);
        Toast.makeText(getBaseContext(),
                "Item Added to Cart",
                Toast.LENGTH_SHORT).show();
        finish();
    }



}









