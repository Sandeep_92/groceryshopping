package com.example.jasma.groceryshop.Views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jasma.groceryshop.Adapters.FruitAdapter;
import com.example.jasma.groceryshop.Models.GroceryModel;
import com.example.jasma.groceryshop.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FruitFragment extends Fragment {
    View view;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    List<GroceryModel> itemInfoArrayList;
    /**
     *
     * @param inflater
     *             inflate the layout for fragment
     * @param container
     *              parent in which layout is inserted
     * @param savedInstanceState
     *        provide information about previous instance
     * @return view for providing user interface
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemInfoArrayList = ShoppingCart.getItemList();
        view = inflater.inflate(R.layout.fragment_fruits, container, false);
        mRecyclerView = view.findViewById(R.id.fruitsList);
        mAdapter = new FruitAdapter(itemInfoArrayList, false);
        LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(getActivity());
        verticalLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(verticalLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }


}

