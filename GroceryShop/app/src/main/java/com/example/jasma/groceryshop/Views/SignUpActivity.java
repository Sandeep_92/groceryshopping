package com.example.jasma.groceryshop.Views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.jasma.groceryshop.Models.UserModel;
import com.example.jasma.groceryshop.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;


public class SignUpActivity extends AppCompatActivity {
    public static final String MY_PREFERENCE = "myPreference";
    Button signUpButton;
    EditText signUpName, signUpPwd, signUpEmail;
    DocumentReference reference;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    UserModel userModel = new UserModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        signUpButton = findViewById(R.id.signupbtn);
        signUpEmail = findViewById(R.id.signup_email);
        signUpName = findViewById(R.id.signup_name);
        signUpPwd = findViewById(R.id.signup_pass);
    }
/*On click method for Create account button*/
    public void createAccount(View view) {
        {
            if (signUpName.getText().toString().isEmpty() || signUpPwd.getText().toString().isEmpty() || signUpEmail.getText().toString().isEmpty()) {
                Toast.makeText(SignUpActivity.this, "Fields can not be null", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignUpActivity.this, SignUpActivity.class);
                startActivity(intent);
            } else {
                reference = FirebaseFirestore.getInstance().collection("Users").document();
                userModel.setName(signUpName.getText().toString().trim());
                userModel.setEmail(signUpEmail.getText().toString().trim());
                userModel.setPassword(signUpPwd.getText().toString().trim());
                userModel.setUserId(reference.getId());
                registerUser(userModel);
            }


        }

    }
/*Creating user table using FireBase*/
    void registerUser(final UserModel user) {
        FirebaseFirestore.getInstance().collection("Users")
                .document(reference.getId())
                .set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    sharedPreferences = getSharedPreferences(MY_PREFERENCE, MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putString("userId", user.getUserId());
                    editor.apply();
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    intent.putExtra("userModel", user);
                    startActivity(intent);
                    finishAffinity();

                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
