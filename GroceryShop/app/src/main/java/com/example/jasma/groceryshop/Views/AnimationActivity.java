package com.example.jasma.groceryshop.Views;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;

public class AnimationActivity {
    private static AnimatorSet animatorSet;
    private static ObjectAnimator animateVertically, animateHorizontally;

   /* method to set animation for fruit adapter using both X and Y axis*/
    public static void animateItems(RecyclerView.ViewHolder groceryHolder, boolean value) {
        animatorSet = new AnimatorSet();
        animateVertically = ObjectAnimator.ofFloat(groceryHolder.itemView, "translationY", value == true ? 100 : -100, 0);
        animateHorizontally = ObjectAnimator.ofFloat(groceryHolder.itemView, "translationX", -70, 70, -40, 40, 0);
        animatorSet.playTogether(animateVertically.setDuration(1500), animateHorizontally.setDuration(1000));
        animatorSet.start();

    }
}
