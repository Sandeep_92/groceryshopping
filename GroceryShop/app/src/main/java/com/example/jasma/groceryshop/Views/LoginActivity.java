package com.example.jasma.groceryshop.Views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jasma.groceryshop.Views.MainActivity;
import com.example.jasma.groceryshop.Models.UserModel;
import com.example.jasma.groceryshop.R;
import com.example.jasma.groceryshop.Views.SignUpActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class LoginActivity extends AppCompatActivity {
    public static final String MY_PREFERENCE = "myPreference";
    String name, password, userIdentity;
    Button loginButton;
    EditText nameText, passwordText;
    TextView signUpTextView;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    ArrayList<UserModel> modelArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.btn_login);
        nameText = findViewById(R.id.log_name);
        passwordText = findViewById(R.id.log_password);
        signUpTextView = findViewById(R.id.textview_signup);
    }
/*FireBase connection of login using FireBase FireStore*/
    public void LoginUser(View view) {
        FirebaseFirestore.getInstance().collection("Users")
                .whereEqualTo("name", nameText.getText().toString().trim())
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot documentSnapshot : task.getResult()) {
                        modelArrayList.add(documentSnapshot.toObject(UserModel.class));
                    }
                    if (modelArrayList.size() == 0) {
                        Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    } else {
                        UserModel userModel = modelArrayList.get(0);
                        for (int i = 0; i < modelArrayList.size(); i++) {
                            name = modelArrayList.get(i).getName();
                            password = modelArrayList.get(i).getPassword();
                            userIdentity = modelArrayList.get(i).getUserId();
                        }
                        if (name != null && password != null) {
                            if (password.matches(passwordText.getText().toString().trim())) {
                                sharedPreferences = getSharedPreferences(MY_PREFERENCE, MODE_PRIVATE);
                                editor = sharedPreferences.edit();
                                editor.putString("userId", userIdentity);
                                editor.apply();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("userModel", userModel);
                                startActivity(intent);
                            } else {

                                Toast.makeText(LoginActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Invalid Login", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

/* on click for signUp activity*/
    public void OnClick(View view) {
        if (view.getId() == R.id.textview_signup) {
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            startActivity(intent);
        }

    }

}
