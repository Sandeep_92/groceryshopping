package com.example.jasma.groceryshop.Views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.jasma.groceryshop.R;

public class SplashActivity extends AppCompatActivity {

    Thread firstScreen = new Thread() {
        @Override
        public void run() {
            try {
                Thread.sleep(2500);
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        firstScreen.start();
    }
}
