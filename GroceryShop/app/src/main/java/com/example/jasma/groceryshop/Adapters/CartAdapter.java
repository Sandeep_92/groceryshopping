package com.example.jasma.groceryshop.Adapters;


import android.content.Context;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.BaseAdapter;

import com.example.jasma.groceryshop.Models.CartModel;
import com.example.jasma.groceryshop.Models.GroceryModel;
import com.example.jasma.groceryshop.R;
import com.example.jasma.groceryshop.Views.ItemDetails;
import com.example.jasma.groceryshop.Views.ShoppingCart;
import com.squareup.picasso.Picasso;


import java.util.List;


public class CartAdapter extends  RecyclerView.Adapter<CartAdapter.ViewHolder> {

    List<GroceryModel> itemInfoList;
    Context context;

    private boolean quantityOrdered;

/* constructor of CartAdapter class  */
    public CartAdapter(List<GroceryModel> itemInfoList, boolean showQuantity) {
        this.itemInfoList = itemInfoList;
        quantityOrdered =showQuantity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_items, parent, false);
        context = parent.getContext();
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }
/* populating data through holder*/
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GroceryModel groceryModel = itemInfoList.get(position);
        holder.tvName.setText(groceryModel.name);
        holder.tvPrice.setText(String.valueOf(groceryModel.price));
        Picasso.with(context).load(groceryModel.image).resize(180, 180).into(holder.imageView);
        if (quantityOrdered) {
            holder.quantity.setText(context.getString(R.string.quantityOrdered) + ShoppingCart.getItemQuantity(groceryModel));
        } else {

            holder.quantity.setText("");
        }

    }
    @Override
    public int getItemCount() {
        return itemInfoList.size();
    }
    /*method to get the views by their ID's*/
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPrice, tvName,quantity;
        ImageView imageView;
        LinearLayout mainLayout;
        TextView totalPrice;
        public ViewHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.cartmainLayout);
            tvPrice = itemView.findViewById(R.id.cartItemprice);
            tvName = itemView.findViewById(R.id.cartviewName);
            imageView=itemView.findViewById(R.id.cartitemImage);
            quantity=itemView.findViewById(R.id.cartquantityOrdered);
            totalPrice=itemView.findViewById(R.id.totalItemPrice);
        }

    }

}