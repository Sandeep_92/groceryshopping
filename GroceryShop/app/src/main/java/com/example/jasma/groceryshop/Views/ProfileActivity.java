package com.example.jasma.groceryshop.Views;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jasma.groceryshop.Models.UserModel;
import com.example.jasma.groceryshop.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {
    EditText nameText, passwordText;
    Button updateButton;
    UserModel userModel = new UserModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        myViews();
    }

    private void myViews() {
        nameText = findViewById(R.id.editName);
        passwordText = findViewById(R.id.editPassword);
        updateButton = findViewById(R.id.updateButton);
        userModel = (UserModel) getIntent().getSerializableExtra("userModel");
        nameText.setText(userModel.getName());
        passwordText.setText(userModel.getPassword());
    }
/*On click method for update button*/
    public void updateData(View view) {
        if (nameText.getText().toString().isEmpty() || passwordText.getText().toString().isEmpty()) {
            Toast.makeText(ProfileActivity.this, "Fill in Required Fields !", Toast.LENGTH_SHORT).show();
        } else {

            userModel.setName(nameText.getText().toString().trim());
            userModel.setPassword(passwordText.getText().toString().trim());
            updateUserData(userModel);
        }
    }
/*Method to update the  data of user*/
    void updateUserData(UserModel user) {
        Map<String, String> mapUser = new HashMap<>();
        mapUser.put("name", nameText.getText().toString().trim());
        mapUser.put("password", passwordText.getText().toString().trim());
        Log.i("testProfile", user.toString());
        FirebaseFirestore.getInstance().collection("Users")
                .document(userModel.getUserId())
                .update("name", nameText.getText().toString().trim(),
                        "password", passwordText.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(ProfileActivity.this, "Data updated", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(ProfileActivity.this, "Some error!", Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }


}
