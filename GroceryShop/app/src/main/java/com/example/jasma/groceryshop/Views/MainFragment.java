package com.example.jasma.groceryshop.Views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jasma.groceryshop.Adapters.FragmentAdapter;
import com.example.jasma.groceryshop.R;

public class MainFragment extends Fragment {
    View view;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentAdapter fragmentAdapter;
    /**
     *
     * @param inflater
     *             inflate the layout for fragment
     * @param container
     *              parent in which layout is inserted
     * @param savedInstanceState
     *        provide information about previous instance
     * @return view for providing user interface
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        toolbar = view.findViewById(R.id.groceryToolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewpager);
        fragmentAdapter = new FragmentAdapter(getFragmentManager());
        fragmentAdapter.addFragments(new FruitFragment(), "ITEMS");
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;


    }
}
