package com.example.jasma.groceryshop.Models;


public class CartModel {
    private GroceryModel groceryModel;
    private int quantity;

    public CartModel(GroceryModel groceryModel, int quantity) {
        this.groceryModel = groceryModel;
        this.quantity = quantity;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        quantity = quantity;
    }

    public GroceryModel getGroceryModel() {
        return groceryModel;
    }

    public void setGroceryModel(GroceryModel groceryModel) {
        this.groceryModel = groceryModel;
    }


}
