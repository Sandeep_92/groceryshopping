package com.example.jasma.groceryshop.Views;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jasma.groceryshop.R;

public class CallActivity extends AppCompatActivity {
    TextView textView;
    Button button1;
    Activity activity = this;
    private static final int REQUEST_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        textView = findViewById(R.id.textNumber);
        button1 = findViewById(R.id.call_Button);
        /* on click for call button*/
        button1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String number = textView.getText().toString();
                Uri call = Uri.parse("tel:" + number);
                Intent callIntent = new Intent(Intent.ACTION_CALL, call);
                startActivity(callIntent);
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_LOCATION);
                } else {
                    startActivity(callIntent);
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}

